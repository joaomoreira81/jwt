const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config(); 


mongoose.connect(process.env.DB_CONNECT,
{ useNewUrlParser: true }, 
() => console.log('Connected to db!'));


//Import Routing
const authRoute = require('./routes/auth');

app.use(express.json());

//Route middlewares
app.use('/api/user', authRoute);

app.listen(8000, () => console.log('JWT Server up and running'));
