const jwt = require('jsonwebtoken');

module.exports = function (req, res, next){
    const token = req.header('jwt-auth-token');
    if(!token) return res.status(401).send('Acesso negado!');

    try{
        const verified = jwt.verify(token,process.env.TOKEN_SECRET);
        req.user = verified;
        next();
    }catch(error){
        res.status(401).send('Token inválido');

    }
}