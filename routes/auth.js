const router = require('express').Router();
const User = require('../model/User');
const { registerValidation,loginValidation } = require('../validation');
const bcrypt = require  ('bcryptjs');
const jwt = require('jsonwebtoken');


router.post('/register', async (req, res) => {

    //Validates fields
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    //Verify username already exists
    const emailExist = await User.findOne({email:req.body.email});
    if(emailExist) return res.status(400).send('E-mail já existe no registo')
    
    
    //HASH the passwords
    const salt = await bcrypt.genSalt(10);
    const hashPwd = await bcrypt.hash(req.body.password,salt);
    

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashPwd,

    });
    try {
        const savedUser = await user.save();
        res.send({user:user._id});
    } catch (err) {
        res.status(400).send.err;
    }
});

router.post('/login', async (req, res) => {
    
    //Validates login params
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    //Verify username already exists
    const user = await User.findOne({email:req.body.email});
    if(!user) return res.status(400).send('E-mail não existe no registo');

    console.log(user.password);
    console.log(req.body.password);
    const validPwd = await bcrypt.compare(req.body.password,user.password);
    console.log(validPwd);
    if(!validPwd) return res.status(400).send('Senha errada');
   
    //Create token JWT
    const token=jwt.sign({_id:user._id}, process.env.TOKEN_SECRET);
    res.header('jwt-auth-token', token).send(token);

    res.send('Logged in!');

});
module.exports = router;